package com.config;

import com.servlet.MyServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.HandlesTypes;
import java.util.Set;

/**
 * @author 龙小虬
 * @date 2021/3/9 16:29
 */
@HandlesTypes(MyHandlersTypes.class)
public class MyServletContainerInitializer implements ServletContainerInitializer {
    /**
     * 容器初始化加载一些操作，手动加载监听器、过滤器，第三方依赖信息
     * @param set 存储继承了MyHandlersTypes类所有子类的Class信息（注意只包含子类）
     * @param servletContext
     * @throws ServletException
     */
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        for (Class<?> s:set) {
            System.out.println(s);
        }
        // 手动加入过滤器
        ServletRegistration.Dynamic myServlet = servletContext.addServlet("myServlet", new MyServlet());
        // 添加过滤器的拦截目标
        myServlet.addMapping("/");
    }
}
