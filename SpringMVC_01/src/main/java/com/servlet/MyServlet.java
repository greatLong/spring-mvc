package com.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 龙小虬
 * @date 2021/3/9 15:23
 */
//@WebServlet("/")
public class MyServlet extends HttpServlet {

    int count = 0;

    public MyServlet(){
        System.out.println("MyServlet无参构造执行。。。");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        count++;
        resp.getWriter().print("this is new Servlet，count:"+count);
    }
}
