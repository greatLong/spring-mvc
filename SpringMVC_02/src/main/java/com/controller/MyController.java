package com.controller;

import com.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.SpringServletContainerInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfoHandlerMapping;

import java.util.concurrent.Callable;

/**
 * @author 龙小虬
 * @date 2021/3/9 17:33
 */
@Controller
//@RestController
public class MyController {

    @Autowired
    AsyncService asyncService;

    // produces = "text/html;charset=UTF-8" 解决中乱码
    @RequestMapping(value = "/",produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String Member(){
        return "解决掉web.xml";
    }


    @RequestMapping(value = "/abc",produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String Member1(){
        return "abc";
    }

    @RequestMapping(value = "/async",produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String async(){
        System.out.println("方法开始调用："+Thread.currentThread().getName());
        String res = asyncService.Test();
        System.out.println("方法结束调用："+Thread.currentThread().getName()+";res:"+res);
        return "AsyncController";
    }

    @RequestMapping(value = "/callable",produces = "text/html;charset=UTF-8")
    @ResponseBody
    public Callable<String> callable(){
        System.out.println("方法开始调用："+Thread.currentThread().getName());
        Callable callable = new Callable<String>(){
            @Override
            public String call() throws Exception {
                System.out.println("call()调用："+Thread.currentThread().getName());
                return asyncService.Test();
            }
        };
        System.out.println("方法结束调用："+Thread.currentThread().getName()+";res: "+callable.toString());
        return callable;
    }
}
