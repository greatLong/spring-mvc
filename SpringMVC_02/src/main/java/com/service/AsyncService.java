package com.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author 龙小虬
 * @date 2021/3/11 15:01
 */
//@Async
@Service
public class AsyncService {

    // 此方法替代很占用时间的处理
    public String Test(){
        try {
            System.out.println("开始休眠："+Thread.currentThread().getName());
            Thread.sleep(3000);
            System.out.println("结束休眠："+Thread.currentThread().getName());
            return "success";
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "fail";
    }
}
