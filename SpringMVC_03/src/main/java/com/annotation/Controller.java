package com.annotation;

import java.lang.annotation.*;

/**
 * @author 龙小虬
 * @date 2021/3/15 10:39
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Controller {
}
