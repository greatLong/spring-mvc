package com.annotation;

import java.lang.annotation.*;

/**
 * @author 龙小虬
 * @date 2021/3/15 10:43
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestMapping {
    String value();
}
