package com.annotation;

import java.lang.annotation.*;

/**
 * @author 龙小虬
 * @date 2021/3/15 10:41
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface ComponentScan {
    String value();
}
