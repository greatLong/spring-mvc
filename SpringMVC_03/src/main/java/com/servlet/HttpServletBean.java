package com.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * @author 龙小虬
 * @date 2021/3/15 10:20
 */
public class HttpServletBean extends HttpServlet {

    @Override
    public void init() throws ServletException {
        initServletBean();
    }

    protected void initServletBean() {
    }

}
