package com.servlet;

import com.method.HandlerMethod;
import com.view.ModelAndView;

import java.lang.reflect.Method;

/**
 * @author 龙小虬
 * @date 2021/3/15 14:32
 */
public class HandlerExecutionChain {
    HandlerMethod handlerMethod;

    public HandlerExecutionChain(HandlerMethod handlerMethod) {
        this.handlerMethod = handlerMethod;
    }

    public ModelAndView handler() throws Exception{
        Method method = handlerMethod.getMethod();
        Object bean = handlerMethod.getBean();
        // 因为我们的contrller只用了string，所以直接强转了
        String invoke = (String)method.invoke(bean, null);
        ModelAndView modelAndView = new ModelAndView(invoke);
        return modelAndView;
    }
}
