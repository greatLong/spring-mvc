package com.method;

import java.lang.reflect.Method;

/**
 * @author 龙小虬
 * @date 2021/3/15 11:32
 */
public class HandlerMethod {
    private  Object bean;

    private  Method method;

    public HandlerMethod(Method method ,Object bean) {
        this.bean = bean;
        this.method = method;
    }

    public Method getMethod() {
        return method;
    }

    public Object getBean() {
        return bean;
    }
}
