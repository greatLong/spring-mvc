package com.config;

import com.annotation.ComponentScan;

/**
 * @author 龙小虬
 * @date 2021/3/15 10:47
 */
@ComponentScan("com.controller")
public class SpringMVCConfig {
}
