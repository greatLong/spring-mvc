package com.web.impl;

import com.servlet.DispatcherServlet;
import com.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * @author 龙小虬
 * @date 2021/3/15 11:10
 */
public class AbstractDispatcherServletInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) {
        ServletRegistration.Dynamic dynamic = servletContext.addServlet("dispatcherServlet", new DispatcherServlet());
        dynamic.addMapping("/");
        System.out.println("反射启动onStartup()方法");
    }
}
