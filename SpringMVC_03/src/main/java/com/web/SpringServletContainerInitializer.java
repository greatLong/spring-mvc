package com.web;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.HandlesTypes;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * @author 龙小虬
 * @date 2021/3/15 11:06
 */
@HandlesTypes(WebApplicationInitializer.class)
public class SpringServletContainerInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        for (Class<?> classInfo : c) {
            try {
                // 使用Java反射技术执行onStartup方法
                Object object = classInfo.newInstance();
                Method onStartup = classInfo.getMethod("onStartup", ServletContext.class);
                onStartup.invoke(object, ctx);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
