package com.web;

import javax.servlet.ServletContext;

/**
 * @author 龙小虬
 * @date 2021/3/15 11:08
 */
public interface WebApplicationInitializer {
    void onStartup(ServletContext servletContext);
}
