package com.controller;

import com.annotation.Controller;
import com.annotation.RequestMapping;

/**
 * @author 龙小虬
 * @date 2021/3/15 10:42
 */
@Controller
public class MyController {

    @RequestMapping("/pay")
    public String test(){
        return "test";
    }
}
