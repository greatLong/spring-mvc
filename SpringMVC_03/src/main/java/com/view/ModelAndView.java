package com.view;

/**
 * @author 龙小虬
 * @date 2021/3/15 14:47
 */
public class ModelAndView {

    private String view;

    public void setView(String view) {
        this.view = view;
    }

    public String getView() {
        return view;
    }

    public ModelAndView(String view) {
        this.view = view;
    }
}
