package com.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author 龙小虬
 * @date 2021/3/18 14:43
 */
@Configuration
@ComponentScan("com.controller")
public class MyConfig {
}
