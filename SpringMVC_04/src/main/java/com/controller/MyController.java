package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 龙小虬
 * @date 2021/3/18 14:40
 */
@Controller
public class MyController {

    @RequestMapping("/myController")
    public String test(){
        return "AbstractHandlerMethodAdapter";
    }
}
